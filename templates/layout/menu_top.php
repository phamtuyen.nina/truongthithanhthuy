<?php
	$d->reset();
	$sql_product_danhmuc="select ten$lang as ten,tenkhongdau,id from #_product_danhmuc where hienthi=1 order by stt,id desc";
	$d->query($sql_product_danhmuc);
	$product_danhmuc=$d->result_array();
?>

<div class="list_menu_sara">
    <p><i class="fa fa-bars" aria-hidden="true"></i> Danh mục sản phẩm</p>
</div>
<div id="menu">
    <div class="giohanga">
        <a href="gio-hang.html">
            <span>Giỏ Hàng</span>
            <img src="images/shopping-cart.svg" alt="Giỏ hàng">
            <b><?=get_total()?></b>
        </a>
    </div>
    <ul>	
        <li><a class="<?php if($_REQUEST['com'] == 'huong-dan-mua-hang') echo 'active'; ?>" href="huong-dan-mua-hang.html">Hướng dẫn mua hàng</a></li>
        <li class="line"></li>
        <li><a class="<?php if($_REQUEST['com'] == 'thuong-hieu') echo 'active'; ?>" href="thuong-hieu.html">Thương hiệu</a></li>
        <li class="line"></li>
        <li><a class="<?php if($_REQUEST['com'] == 'thong-tin-tieu-dung') echo 'active'; ?>" href="thong-tin-tieu-dung.html">Thông tin tiêu dùng</a></li>
        <li class="line"></li>
        <li><a class="<?php if($_REQUEST['com'] == 'lien-he') echo 'active'; ?>" href="lien-he.html"><?=_lienhe?></a></li>
    </ul>

</div>

<!-- <div id="search">
    <input type="text" name="keyword" id="keyword" onKeyPress="doEnter(event,'keyword');" value="<?=_nhaptukhoatimkiem?>..."  onclick="if(this.value=='<?=_nhaptukhoatimkiem?>...'){this.value=''}" onblur="if(this.value==''){this.value='<?=_nhaptukhoatimkiem?>...'}">
    <i class="fa fa-search" aria-hidden="true" onclick="onSearch(event,'keyword');"></i>
</div> -->