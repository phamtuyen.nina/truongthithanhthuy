<?php	

	$d->reset();
	$sql_contact = "select noidung$lang as noidung from #_about where type='footer' limit 0,1";
	$d->query($sql_contact);
	$company_contact = $d->fetch_array();

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau from #_news where type='chinh-sach' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $chinhsach = $d->result_array();

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau from #_news where type='ho-tro-khach-hang' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $hotrokhachhang = $d->result_array();

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau from #_news where type='dich-vu-chung-toi' and hienthi=1 order by stt,id desc";
    $d->query($sql);
    $dichvuchungtoi = $d->result_array();    

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='lienket' order by stt,id desc";
    $d->query($sql);
    $lienket=$d->result_array();        
?>
<div id="wap_footer" class="lazy" data-src="images/bg_ft.jpg">
    <div class="wapper">
        <div class="row1">
            <div class="col-md-4 col-main">
                <p class="title_footer">Thông tin về chúng tôi</p>
                <div class="content_zz">
                    <?=$company_contact['noidung'];?>
                </div>
                <div class="nhantin">
                    <?php include _template."layout/dangkynhantin.php";?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row1">
                    <div class="col-md-4 col-main1">
                        <p class="title_footer">Chính sách</p>
                        <ul>
                            <?php foreach ($chinhsach as $v) {?>
                            <li><a href="chinh-sach/<?=$v['tenkhongdau']?>.html">+ <?=$v['ten']?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-4 col-main1">
                        <p class="title_footer">Hỗ trợ khách hàng</p>
                        <ul>
                            <?php foreach ($hotrokhachhang as $v) {?>
                            <li><a href="ho-tro-khach-hang/<?=$v['tenkhongdau']?>.html">+ <?=$v['ten']?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-md-4 col-main1">
                        <p class="title_footer">Dịch vụ chúng tôi</p>
                        <ul>
                            <?php foreach ($dichvuchungtoi as $v) {?>
                            <li><a href="dich-vu-chung-toi/<?=$v['tenkhongdau']?>.html">+ <?=$v['ten']?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-copy row1 clearfix">
                        <div class="copyzz col-md-7">
                            © 2019 Trương Thị Thanh Thúy - Thiết kế Web : NiNa Co., Ltd 
                        </div>
                        <div class="lienket col-md-5 text-right">
                            <?php foreach ($lienket as $v) {?>
                            <a href="<?=$v['link']?>" target="_blank" rel="nofollow">
                                <img src="<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                            </a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
