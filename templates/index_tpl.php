<?php 
    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,photo,ngaytao,mota$lang as mota from #_news where type='thong-tin-tieu-dung' and hienthi=1 and noibat=1 order by stt,id desc";
    $d->query($sql);
    $tttieudung = $d->result_array();

    $d->reset();
    $sql = "select ten$lang as ten,link,photo,mota$lang as mota from #_slider where hienthi=1 and type='xuhuong' order by stt,id desc";
    $d->query($sql);
    $xuhuong=$d->result_array(); 

    $d->reset();
    $sql = "select id,ten$lang as ten,tenkhongdau,banner,photo,gia,giakm,mota$lang as mota from #_product where type='san-pham' and hienthi=1 and noibat=1 order by stt,id desc";
    $d->query($sql);
    $product = $d->result_array();

    $d->reset();
    $sql="select ten$lang as ten,tenkhongdau,id,photo from #_product_danhmuc where hienthi=1 and type='san-pham' and noibat=1 order by stt,id desc";
    $d->query($sql);
    $product_danhmuc=$d->result_array();           
?>
<div class="wap_xuhuong">
    <div class="wapper">
        <div class="title_page">
            <h4>Xu hướng tìm kiếm</h4>
        </div>
        <div class="show_xuhuong">
            <?php foreach ($xuhuong as $v) {?>
            <p>
                <a class="lazyload" href="<?=$v['link']?>" target="_blank">
                    <img class="lazy" data-src="thumb/397x153x1x90/<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                </a>
            </p>
            <p>
                <a class="lazyload" href="<?=$v['link']?>" target="_blank">
                    <img class="lazy" data-src="thumb/397x153x1x90/<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                </a>
            </p>
            <p>
                <a class="lazyload" href="<?=$v['link']?>" target="_blank">
                    <img class="lazy" data-src="thumb/397x153x1x90/<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                </a>
            </p>
            <p>
                <a class="lazyload" href="<?=$v['link']?>" target="_blank">
                    <img class="lazy" data-src="thumb/397x153x1x90/<?=_upload_hinhanh_l.$v['photo']?>" alt="<?=$v['ten']?>">
                </a>
            </p>
            <?php }?>
        </div>
    </div>
</div>
<div class="wap_noibat">
    <div class="wapper">
        <div class="title_page">
            <h4>Sản phẩm nổi bật</h4>
        </div>
        <div class="whow_noibat">
            <?php foreach ($product as $v) {?>
            <div class="pad_noibat">
                <div class="col-img">
                    <a href="san-pham/<?=$v['tenkhongdau']?>.html">
                        <img src="<?=_upload_sanpham_l.$v['banner']?>" alt="<?=$v['ten']?>">
                    </a>
                </div>
                <div class="col-info">
                    <div class="pad_info">
                        <h3>
                            <a href="san-pham/<?=$v['tenkhongdau']?>.html"><?=$v['ten']?></a>
                        </h3>
                        <div class="mm_mota"><?=catchuoi(trim(strip_tags($v['mota'])),130)?></div>
                        <div class="mm_s_gia">
                            <?php if($v['giakm']>0){?>
                            <p><?=number_format($v['giakm'],0, ',', '.')?>đ</p>
                            <p><?=number_format($v['gia'],0, ',', '.')?>đ</p>
                            <?php }else{?>
                            <p><?php if($v['gia']!=0) echo number_format($v['gia'],0, ',', '.').'đ'; else echo 'Liên hệ'; ?></p>
                            <?php }?>
                        </div>
                        <div class="chitiet_s text-right">
                            <p>
                                <a href="javascript:void(0)">Mua ngay</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>

<div class="wap_tab">
    <div class="wapper">
        <div class="title_tab">
            <ul class="ul_tab1">
                <li data-t="spmoi" data-link="san-pham-moi.html"><span>Sản phẩm mới</span></li>
                <li data-t="spbanchay" data-link="san-pham-ban-chay.html"><span>Sản phẩm bán chạy</span></li>
                <li data-t="khuyenmai" data-link="san-pham-khuyen-mai.html"><span>Sản phẩm khuyến mãi</span></li>
            </ul>
            <a class="chuyenlink1" href="san-pham-moi.html">Xem thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
        </div>
        <div class="show_tab">
            
        </div>
    </div>
</div>

<div>
    <?php foreach ($product_danhmuc as $c) {
        $d->reset();
        $sql="select ten$lang as ten,tenkhongdau,id,photo from #_product_list where hienthi=1 and type='san-pham' and id_danhmuc=".$c['id']." and noibat=1 order by stt,id desc";
        $d->query($sql);
        $product_list=$d->result_array();
    ?>
    <div class="wap_danhmuc_t">
        <div class="wapper">
            <div class="title_danhmuc_list">
               <ul class="ul_tab_dm">
                   <li data-link="san-pham/<?=$c['tenkhongdau']?>"><span><?=$c['ten']?></span></li>
                   <?php foreach ($product_list as $l) {?>
                    <li data-link="san-pham/<?=$l['tenkhongdau']?>/"><span><?=$l['ten']?></span></li>
                   <?php }?>
               </ul> 
               <a class="chuyenlink1" href="san-pham/<?=$c['tenkhongdau']?>">Xem thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <?php }?>
</div>

<div class="wap_tintuc lazy" data-src="images/bg_tin.jpg">
    <div class="wapper">
        <div class="title_page white">
            <h3>Bí quyết chăm sóc sức khỏe</h3>
        </div>
        <div class="row">
            <div class="slick_tin_t">
                <?php foreach ($tttieudung as $v) {?>
                <div class="col-xs-4">
                    <div class="pad_tin">
                        <a href="thong-tin-tieu-dung/<?=$v['tenkhongdau']?>.html">
                            <img onError="this.src='http://placehold.it/380x250';" src="thumb/380x250x1x90/<?=_upload_tintuc_l.$v['photo']?>" alt="<?=$v['ten']?>">
                        </a>
                        <div class="pad_info_">
                            <div class="ngaytaotin">
                                <span>Tháng <?=date('m',$v['ngaytao'])?></span>
                                <span><?=date('d',$v['ngaytao'])?></span>
                            </div>
                            <div class="tt_tintuc">
                                <h4>
                                    <a href="thong-tin-tieu-dung/<?=$v['tenkhongdau']?>.html"><?=$v['ten']?></a>
                                </h4>
                                <div><?=catchuoi(trim(strip_tags($v['mota'])),100)?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<div class="wap_thuonghieu">
    <div class="wapper">
        <div class="title_page">
            <h4>Thương hiệu</h4>
        </div>
    </div>
</div>